FROM python
RUN mkdir /var/check-3

WORKDIR /var/check-3

COPY ./* ./

CMD [ "./main" ]